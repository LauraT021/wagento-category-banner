<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Wagento\Banners\Helper;

use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\ObjectManagerInterface;

class Output extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $scopeConfig;
    protected $collectionFactory;
    protected $objectManager;
        
   
    public function __construct( 
        \Magento\Framework\App\Helper\Context $context,
        \Wagento\Banners\Model\ResourceModel\Banners\CollectionFactory $collectionFactory,
        ObjectManagerInterface $objectManager
    ) {

        $this->scopeConfig = $context->getScopeConfig();
        $this->collectionFactory = $collectionFactory;
        $this->objectManager = $objectManager;
        parent::__construct($context);
    }

 
    public function getFrontBanners(){                   
        $collection = $this->collectionFactory->create()->addFieldToFilter('status', 1);
        return $collection;
    }

    public function getFrontBannersArray(){                   
        $collection = $this->collectionFactory->create()->addFieldToFilter('status', 1);
        return $collection->toArray();
    }
         

    public function getSingleBanner($id = 0 ){

        $item = null; 
        $collection = $this->collectionFactory->create()->addFieldToFilter('status', 1)->addFieldToFilter('banners_id', $id);

        foreach($collection as $index => $c){ 
            $item = $c; 
        }
                                
        return $item;
    }       
        
    public function getBannerBlockArguments(){
            
        $list =  $this->getBannerList();
                
        $listArray = [];
                
        if ($list != '') {
            $listArray = explode(',', $list);
        }
                
        return $listArray;
    }
        
    public function getMediaDirectoryUrl(){
            
        $media_dir = $this->objectManager->get('Magento\Store\Model\StoreManagerInterface')
        ->getStore()
        ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            
        return $media_dir;
    }
}