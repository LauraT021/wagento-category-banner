<?php

namespace Wagento\Banners\Model;

class Banners extends \Magento\Framework\Model\AbstractModel
{
                
    protected function _construct()
    {
        $this->_init('Wagento\Banners\Model\ResourceModel\Banners');
    }
        
        
    public function getAvailableStatuses()
    {                                
        $availableOptions = ['1' => 'Enable',
                          '0' => 'Disable'];
                
        return $availableOptions;
    }

    public function getAvailablePositions()
    {                                
        $availableOptions = [
                                '1' => 'Right',
                                '2' => 'Left',
                                '3' => 'Only Image'
                            ];
                
        return $availableOptions;
    }
}
